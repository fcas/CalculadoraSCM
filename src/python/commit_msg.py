#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Felipe'

import requests
import json
import sys
import re


def is_commit_valid(requested_id):

    try:
        r = requests.get('http://localhost:3000/issues/' + str(requested_id) + '.json'
                         , auth=('user', 'pass'))
        if r.status_code == 404:
            print "A issue informada não existe"
            sys.exit(1)
    except requests.exceptions.RequestException as e:
        print e
        sys.exit(1)

    response = json.loads(r.content)
    issue_status = response['issue']['status']['name']
    issue_id = response['issue']['id']

    if issue_status != "In Progress":
        print "A issue não está com status: Em progresso"
        sys.exit(1)
    else:
        print "Issue: " + str(issue_id) + " Status: " + issue_status
        sys.exit(0)


def extract_id(message):
    substring = re.search("\\brefs\\b #\\d+", message)
    if substring:
        issue_ref = substring.group(0).partition("#")[2]
        is_commit_valid(issue_ref)
    else:
        print "Referencie a issue utilizando refs #numero_da_issue"
    sys.exit(1)


if __name__ == '__main__':

    commit_message_file = open(sys.argv[1])
    commit_message = commit_message_file.read().strip()
