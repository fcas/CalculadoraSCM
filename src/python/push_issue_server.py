#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Felipe'

from flask import request, Flask
import requests
import json
import sys
import re

app = Flask(__name__)


@app.route('/github_hook.json', methods=['POST'])
def foo():

    data = json.loads(request.data)

    if data['object_kind'] == "push":
        handle_push(data)

    if data['object_kind'] == "issue":
        handle_issue(data['object_attributes'])


def handle_issue(data):
    try:
        if data['state'] == "opened":
            status_id = 1
            subject = data['title']
            description = data['description']

            d = {'issue': {'project_id': 1,
                           'subject': subject,
                           'description': description,
                           'priority_id': 2,
                           'status_id': status_id
                           }
                 }

            requests.post('http://localhost:3000/issues.json',
                          data=json.dumps(d), headers={'Content-Type': 'application/json'},
                          params={'key': '2bb3a2c3313109c217fb5855f9d691b2224966c1'})

    except requests.exceptions.RequestException as e:
        print e
        sys.exit(1)


def handle_push(data):
    try:

        commit = data['commits'][0]
        message = commit['message']
        timestamp = commit['timestamp']
        author = commit['author']['name']
        files_added = read_file_names(data['added'])
        files_removed = read_file_names(data['removed'])
        files_modified = read_file_names(data['modified'])

        d = {'issue': {'notes': '*Date:* ' + timestamp +
                                ' *User:* ' + author +
                                " *Files added:* " + files_added +
                                " *Files removed:* " + files_removed +
                                " *Files modified:* " + files_modified +
                                " *Comments:* " + message
                       }
             }

        requests.put('http://localhost:3000/issues/' + str(extract_id(message)) + '.json', data=json.dumps(d),
                     headers={'Content-Type': 'application/json'},
                     params={'key': '2bb3a2c3313109c217fb5855f9d691b2224966c1'})

    except requests.exceptions.RequestException as e:
        print e
        sys.exit(1)


def extract_id(message):
    substring = re.search("\\brefs\\b #\\d+", message)
    if substring:
        issue_ref = substring.group(0).partition("#")[2]
    return issue_ref


def read_file_names(names):
    file_names = ""
    for idx, name in enumerate(names):
        if idx % 2 != 0:
            file_names = file_names + "source:" + name + ","
        else:
            file_names = file_names + "source:" + name
    return file_names

if __name__ == '__main__':
    app.run(debug=True, host='127.0.0.1', port=4569)

